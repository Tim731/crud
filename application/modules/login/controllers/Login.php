<?php

    class Login extends MY_Controller{

        function __construct(){
            parent::__construct();    
        }

        function login(){
            // view page path
            $data['content_view'] = 'Login/Login_view';
            // data to pass in view page
            $data['content_data'] = '';
            // function to load view
            $this->load->view($data['content_view']);
        }

        function signup(){
            // view page path
            $data['content_view'] = 'Login/Signup_view';
            // data to pass in view page
            $data['content_data'] = '';
            // function to load view
            $this->load->view($data['content_view']);
        }

        function signup_request(){
            //  get post request data
            $email = $this->input->post("email");
            $pass = $this->input->post("pass");
            $name = $this->input->post("name");
           
            // function to load model
            $this->load->model('Login_model');
            // Calling class and pass to variable
            $user_m = $this->Login_model;

            // check if email already in database
            $where = array('user_email' => $email);
            $user = $user_m->get_user($where);

            // create array: [key] = table name; [value] = request data
            $data = array('name' => $name, 'user_email' => $email, "user_pass" => $pass);

            
            if(count($user) == 0){
                // call function for add data
                $user_m->add_user($data);

                $title = "Success... ";
                $content = "User successfuly added";
            }else{
                $title = "Sorry... ";
                $content = "Email is already taken";
            }

            $array = array(
                'title' => $title,
                'content' => $content
            );

            echo json_encode($array);

        }

        function login_request(){
            // function to load model
            $this->load->model('Login_model');
            // Calling class and pass to variable
            $user_m = $this->Login_model;

            $email = $this->input->post("email");
            $pass = $this->input->post("pass");

            $where = array('user_email' => $email, 'user_pass' => $pass);
            $user = $user_m->get_user($where);

            if(count($user) == 1){

                $title = "Success... ";
                $content = "User successfuly login";
            }else{
                $title = "Sorry... ";
                $content = "Invalid email or password";
            }

            $array = array(
                'title' => $title,
                'content' => $content
            );

            echo json_encode($array);

        }
    }

?>