<?php
class Login_model extends CI_Model{
    function __construct(){
        parent::__construct();
    }

    public function get_users(){
    // get all user
        $user = $this->db->select('*')
        ->from('users')
        ->get();

        return $user->result();
    }

    public function get_user($where){
        $user = $this->db->select('*')
        ->from('users')
        ->where($where)
        ->get();

        return $user->result();
    }

    public function add_user($data){
        $this->db->insert('users', $data);
    }

    public function delete_user($id){
        $this->db->where(array('user_id' => $id));
        return  $this->db->delete('users');
    }

    public function edit_user($id, $data){
        $this->db->where(array('user_id' => $id));
        return $this->db->update('users', $data);
    }
    
}
?>